#!/bin/bash

cd ~

# setup nginx with gunicorn - https://panel.djangoeurope.com/faq/#gunicorn
mkdir -p ~/init
if [ ! -f ~/init/nginx ]; then
    wget https://templates.wservices.ch/nginx/nginx.init -O ~/init/nginx
    chmod 755 ~/init/nginx
fi
mkdir -p ~/nginx
mkdir -p ~/nginx/conf
if [ ! -f ~/nginx/conf/nginx.conf ]; then
    wget https://templates.wservices.ch/nginx/nginx.conf -O ~/nginx/conf/nginx.conf
fi
mkdir -p ~/nginx/conf/sites
mkdir -p ~/nginx/temp
mkdir -p ~/nginx/logs


DJANGO_BASE_PROJECT_REPOSITORY_URL=https://bitbucket.org/fabiocaccamo/django-base-project

GIT_USER_EMAIL="fabio.caccamo@gmail.com"
GIT_USER_NAME="Fabio Caccamo"

PROJECT_ENVS=(dev prod)
PROJECTS_DIR=~/projects
PROJECT_NAME=$1
PROJECT_PATH=$PROJECTS_DIR/$PROJECT_NAME
PROJECT_REPOSITORY_URL=$2


if [ -z "$PROJECT_NAME" ]; then
    echo "project name cannot be empty"
    cd ~
    return
fi

if [ "${#PROJECT_NAME}" -lt 3 ]; then
    echo "project name too short, at least 3 chars"
    cd ~
    return
fi

if [[ "$PROJECT_NAME" =~ [^a-zA-Z0-9\_] ]]; then
    echo "invalid project name: '$PROJECT_NAME'"
    cd ~
    return
fi

if [ -d "$PROJECT_NAME" ]; then
    echo "project name already in use"
    cd ~
    return
fi

if [ -z "$PROJECT_REPOSITORY_URL" ]; then
    echo "project repository url cannot be empty"
    cd ~
    return
fi


mkdir -p $PROJECTS_DIR && cd $PROJECTS_DIR
mkdir $PROJECT_NAME && cd $PROJECT_NAME


for ENV in ${PROJECT_ENVS[*]}
do
    PROJECT_ENV_NAME=${PROJECT_NAME}_${ENV}
    PROJECT_ENV_PATH=${PROJECT_PATH}/${PROJECT_ENV_NAME}

    if [ ! -d "$PROJECT_ENV_NAME" ]; then

        echo "create virtualenv named '$PROJECT_ENV_NAME'"

        virtualenv --no-site-packages $PROJECT_ENV_NAME
        cd $PROJECT_ENV_NAME
        source bin/activate

        pip install pip --upgrade
        pip install -r ~/new-project-requirements.txt
        pip freeze

        mkdir cache

        mkdir env
        touch env/${ENV}

        mkdir logs

        mkdir private

        DJANGO_SECRET_KEY=$(python -c 'import random; print "".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)") for i in range(50)])')
        echo "DJANGO_SECRET_KEY = '$DJANGO_SECRET_KEY'"
        echo "$DJANGO_SECRET_KEY" > private/DJANGO_SECRET_KEY
        touch private/DJANGO_DATABASE_NAME
        touch private/DJANGO_DATABASE_USER
        touch private/DJANGO_DATABASE_PASSWORD

        mkdir public
        mkdir public/static
        mkdir public/media

        mkdir sources
        cd sources

        # download and extract django-base-project from .git repo
        DJANGO_BASE_PROJECT_LAST_COMMIT_HASH=$(git ls-remote -h $DJANGO_BASE_PROJECT_REPOSITORY_URL/.git | cut -f 1)
        # echo "DJANGO_BASE_PROJECT_LAST_COMMIT_HASH = $DJANGO_BASE_PROJECT_LAST_COMMIT_HASH"
        # echo "now download zip of django-base-project at commit $DJANGO_BASE_PROJECT_LAST_COMMIT_HASH"
        DJANGO_BASE_PROJECT_TARBALL=$DJANGO_BASE_PROJECT_LAST_COMMIT_HASH.tar.gz
        DJANGO_BASE_PROJECT_TARBALL_URL=$DJANGO_BASE_PROJECT_REPOSITORY_URL/get/$DJANGO_BASE_PROJECT_TARBALL
        # echo "download base project archive from url: $DJANGO_BASE_PROJECT_TARBALL_URL"
        wget $DJANGO_BASE_PROJECT_TARBALL_URL
        tar -xvf $DJANGO_BASE_PROJECT_TARBALL --strip 1
        rm -rf $DJANGO_BASE_PROJECT_TARBALL

        cd ..

        # autodetect python path
        PYTHON_PATH=""
        PYTHON_PATHNAME=""

        for i in "lib"/*; do
            if [ -d "$i" ];
            then
                python_path_temp=$i
                python_pathname_temp=${python_path_temp##*/}

                if [[ "$python_pathname_temp" =~ ^python[0-9]+(\.[0-9]+){1}?$ ]];
                then
                    PYTHON_PATH=$python_path_temp
                    PYTHON_PATHNAME=$python_pathname_temp
                    # echo $python_pathname
                fi
            fi
        done

        if [ -z "$PYTHON_PATH" ];
        then
            echo "python path not founded"
            cd ~
            return
        else
            echo "python path founded: $PYTHON_PATH"
        fi


        # create project-env gunicorn init script in ~/init
        PROJECT_INIT_SCRIPT_TEMPLATE=~/new-project-gunicorn.init
        PROJECT_INIT_SCRIPT=~/init/$PROJECT_ENV_NAME
        cp -fv $PROJECT_INIT_SCRIPT_TEMPLATE $PROJECT_INIT_SCRIPT
        sed -i "s|__PROJECT_ENV_NAME__|${PROJECT_ENV_NAME}|g" $PROJECT_INIT_SCRIPT
        sed -i "s|__PROJECT_ENV_PATH__|${PROJECT_ENV_PATH}|g" $PROJECT_INIT_SCRIPT
        sed -i "s|__PYTHON__|${PYTHON_PATHNAME}|g" $PROJECT_INIT_SCRIPT
        chmod 755 $PROJECT_INIT_SCRIPT
        $PROJECT_INIT_SCRIPT start


        # create nginx site conf ONLY for dev env
        if [ $ENV == ${PROJECT_ENVS[0]} ]; then
            PROJECT_CONF_TEMPLATE=~/new-project-nginx.conf
            PROJECT_CONF=~/nginx/conf/sites/$PROJECT_ENV_NAME.conf
            cp -fv $PROJECT_CONF_TEMPLATE $PROJECT_CONF
            sed -i "s|__PROJECT_ENV_NAME__|${PROJECT_ENV_NAME}|g" $PROJECT_CONF
            sed -i "s|__PROJECT_ENV_PATH__|${PROJECT_ENV_PATH}|g" $PROJECT_CONF
            sed -i "s|__PROJECT_NAME__|${PROJECT_NAME}|g" $PROJECT_CONF
            # TODO: port in .conf should be dynamic
        fi

        deactivate
        cd ../

    else
        echo "virtualenv named '$PROJECT_ENV_NAME' already created"
    fi
done

# create projects activation script
touch ~/activate-project.sh
cat <<EOF > ~/activate-project.sh
#!/bin/bash

PROJECTS_DIR=~/projects
PROJECT_NAME=\$1
PROJECT_PATH=\${PROJECTS_DIR}/\${PROJECT_NAME}

ENV=\$2

if [ -z "\$ENV" ]; then
    ENV=dev
fi

PROJECT_ENV_NAME=\${PROJECT_NAME}_\${ENV}
PROJECT_ENV_PATH=\${PROJECT_PATH}/\${PROJECT_ENV_NAME}

cd \$PROJECT_ENV_PATH
source bin/activate
cd sources
EOF

# create project activation shortcut
mkdir -p ~/activate/
rm -rf ~/activate/$PROJECT_NAME.sh
touch ~/activate/$PROJECT_NAME.sh
cat <<EOF >> ~/activate/$PROJECT_NAME.sh
#!/bin/bash

source ~/activate-project.sh $PROJECT_NAME \$1
EOF


# setup git for project envs
echo "git setup start with repo at $PROJECT_REPOSITORY_URL"

for ENV in ${PROJECT_ENVS[*]}
do
    PROJECT_ENV_NAME=${PROJECT_NAME}_${ENV}
    PROJECT_ENV_PATH=${PROJECT_PATH}/${PROJECT_ENV_NAME}

    cd $PROJECT_ENV_PATH

    if [ $ENV == ${PROJECT_ENVS[0]} ]; then

        echo "push git repo: $ENV to git"

        cd sources

        git init
        git remote add origin $PROJECT_REPOSITORY_URL

        git config user.email $GIT_USER_EMAIL
        git config user.name $GIT_USER_NAME

        touch .gitignore
        echo .DS_Store >> .gitignore
        echo *.pyc >> .gitignore
        echo *.pyo >> .gitignore

        git add .gitignore
        git commit -m "Added .gitignore"
        git push -u origin master

        git add -A
        git commit -m "Added sources"
        git push -u origin master

        cd ../

    else
        echo "clone git repo: git to $ENV"

        rm -rf sources

        git clone $PROJECT_REPOSITORY_URL sources
    fi

    cd ../

done
echo "git setup complete"


echo "- follow these steps:"
echo "    - create domains, websites and databases"
echo "    - config DJANGO_DATABASE_NAME, DJANGO_DATABASE_USER and DJANGO_DATABASE_PASSWORD in private/ folder for all envs"
echo "    - config settings"
echo "    - config site port in nginx/conf/sites/"
echo "\n"


while true; do

    read -p "Have you done all the steps listed above? Continue setup? [Y/n]" yn

    case $yn in

        [Yy]* )

            for ENV in ${PROJECT_ENVS[*]}
            do
                PROJECT_ENV_NAME=${PROJECT_NAME}_${ENV}
                PROJECT_ENV_PATH=${PROJECT_PATH}/${PROJECT_ENV_NAME}

                cd $PROJECT_ENV_PATH
                source bin/activate

                cd sources

                if [ $ENV == ${PROJECT_ENVS[0]} ]; then

                    git add -A
                    git status
                    git commit -m "Updated settings"
                    git push -u origin master
                else
                    git pull
                    git reset --hard origin/master
                fi

                python manage.py migrate
                python manage.py collectstatic --clear --noinput
                python manage.py createsuperuser

                ~/init/$PROJECT_ENV_NAME restart

                deactivate

            done

            cd ~
            chmod u+x init/nginx
            init/nginx reload

            source activate/$PROJECT_NAME.sh ${PROJECT_ENVS[0]}

            break
            ;;

        [Nn]* )

            echo "- follow these steps for each virtualenv:"
            echo "    - cd into virtualenv and activate it"
            echo "    - python manage.py migrate"
            echo "    - python manage.py collectstatic --clear --noinput"
            echo "    - python manage.py createsuperuser"
            echo "    - restart appserver"
            echo "- reload webserver"

            break
            ;;

        * )

            echo "please answer yes or no."
            ;;

    esac

done