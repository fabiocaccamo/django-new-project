#!/bin/bash

USERNAME=chiarazavattaro
PROJECT=$1
VIRTUALENV=$2

if [ -z "$PROJECT" ]; then
    echo "project name cannot be empty"
    return
fi

if [ -z "$VIRTUALENV" ]; then
    VIRTUALENV=dev
fi

#echo $PROJECT
#echo $VIRTUALENV

ps aux | grep $USERNAME | grep $PROJECT | grep $VIRTUALENV | awk '{print $2}' | xargs kill -9