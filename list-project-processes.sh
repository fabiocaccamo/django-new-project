#!/bin/bash

USERNAME=chiarazavattaro
FILTER=$1

if [ -z "$FILTER" ]; then
    
    echo [DEV]
    ps aux --sort command | grep $USERNAME | grep dev | awk '{print "    "$14" -> "$2}' | sort
    echo [/DEV]
    
    echo [PROD]
    ps aux --sort command | grep $USERNAME | grep prod | awk '{print "    "$14" -> "$2}' | sort
    echo [/PROD]
    
else
    
    echo [DEV]
    ps aux --sort command | grep $USERNAME | grep $FILTER | grep dev | awk '{print "    "$14" -> "$2}' | sort
    echo [/DEV]
    
    echo [PROD]
    ps aux --sort command | grep $USERNAME | grep $FILTER | grep prod | awk '{print "    "$14" -> "$2}' | sort
    echo [/PROD]
    
fi